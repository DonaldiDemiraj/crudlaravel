<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>Student Management System</title>
  </head>
  <body>

    @include("navbar")

    <div class="row header-container justify-content-center">
        <div class="header">
            <h1>Student Management System</h1>
        </div>
    </div>


    @if ($layout == 'index')
        <div class="container-fluid mt-4">
            <div class="container-fluid mt-4">
                <div class="row justify-content-center">
                    <section class="col-md-7" style="margin-top: 40px;">
                        @include("studentslist")
                    </section>
                    <section class="col-md-5"></section>
                </div>
            </div>
        </div>
    @elseif($layout == 'create')
        <div class="container-fluid">
            <div class="row">
                <section class="col mt-4" style="margin-top: 40px;">
                    @include("studentslist")
                </section>
                <section class="col-md-5" style="margin-top: 40px;">

                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Enter the information for new student</h5>
                            <form action="{{ url('/store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>CNE</label>
                                    <input type="text" name="cne" class="form-control" placeholder="Enter Your CNE">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstName" class="form-control" placeholder="Enter Your FirstName">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="secondName" class="form-control" placeholder="Enter Your LastName">
                                </div>
                                <div class="form-group">
                                    <label>Age</label>
                                    <input type="text" name="age" class="form-control" placeholder="Enter Your Age">
                                </div>
                                <div class="form-group">
                                    <label>Speciality</label>
                                    <input type="text" name="speciality" class="form-control" placeholder="Enter Your Speciality">
                                </div>
                                <input type="submit" class="btn btn-info" value="Save">
                                <input type="reset" class="btn btn-warning" value="Reset">
                            </form>
                        </div>
                    </div>


                    
                </section>
            </div>
        </div>
    @elseif($layout == 'show')
        <div class="container-fluid">
            <div class="row">
                <section class="col-md-7 mt-4">
                    @include("studentslist")
                </section>
                <section class="col-md-5"></section>
            </div>
        </div>
    @elseif($layout == 'edit')
        <div class="container-fluid mt-4">
            <div class="row">
                <section class="col">
                    @include("studentslist")
                </section>
                <section class="col-md-5">

                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Update information of students</h5>
                            <form action="{{ url('/update/'.$student->id)}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>CNE</label>
                                    <input type="text" value="{{$student->cne}}" name="cne" class="form-control" placeholder="Enter Your CNE">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" value="{{$student->firstName}}" name="firstName" class="form-control" placeholder="Enter Your FirstName">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" value="{{$student->secondName}}" name="secondName" class="form-control" placeholder="Enter Your LastName">
                                </div>
                                <div class="form-group">
                                    <label>Age</label>
                                    <input type="text" value="{{$student->age}}" name="age" class="form-control" placeholder="Enter Your Age">
                                </div>
                                <div class="form-group">
                                    <label>Speciality</label>
                                    <input type="text" value="{{$student->speciality}}" name="speciality" class="form-control" placeholder="Enter Your Speciality">
                                </div>
                                <input type="submit" class="btn btn-info" value="Update">
                                <input type="reset" class="btn btn-warning" value="Reset">
                            </form>
                        </div>
                    </div>

                    
                </section>
            </div>
        </div>
    @endif


    <footer>

    </footer>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>